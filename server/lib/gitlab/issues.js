var q = require('q'),
    _ = require('lodash'),
    issues = module.exports = function issues(client, projects, formatter, container) {
        this.client = client;
        this.projects = projects;
        this.formatter = formatter;
        this.container = container;
    };

issues.prototype = {
    url: function(namespace, project, id) {
        if (typeof project === "number") {
            id = project;
            project = null;
        }

        var base = this.projects.url(namespace, project) + '/issues';

        if (!id) {
            return base;
        }

        return base + '/' + id;
    },

    one: function(token, namespace, project, id) {
        var url = this.url(namespace, project, id),
            formatter = this.formatter,
            timerGet = this.container.get('statsd').createTimer('gitlab.get'),
            timerRoute = this.container.get('statsd').createTimer('gitlab.get.issue');

        return this.client.get(token, url)
            .then(function(response) {
                timerGet.stop();
                timerRoute.stop();

                if (response.statusCode !== 200) {
                    throw response;
                }

                return formatter.formatIssueFromGitlab(response.body);
            });
    },

    all: function(token, namespace, project) {
        var url = this.url(namespace, project),
            deferred = q.defer(),
            formatter = this.formatter,
            timerFactory = this.container.get('statsd').createTimer.bind(this.container.get('statsd')),
            client = this.client,
            fetch = function(issues, page, state, labels) {
                var timerGet = timerFactory('gitlab.get'),
                    timerRoute = timerFactory('gitlab.get.issues'),
                    params = {};

                params.page = ++page;
                params.state = state || 'opened';
                params.per_page = 50;

                if (labels) {
                    params.labels = labels.join(',');
                }

                return client.get(token, url, params)
                    .then(function(response) {
                        timerGet.stop();
                        timerRoute.stop();

                        issues = issues.concat(response.body.map(formatter.formatIssueFromGitlab));

                        if (response.links.next) {
                            return fetch(issues, page, params.state, labels);
                        }

                        return issues;
                    });
            };

        return q.all([fetch([], 0), fetch([], 0, 'closed', [this.container.get('config').board_prefix + 'starred'])])
            .spread(function(issues, starred) {
                return issues.concat(starred);
            })
            .then(function(issues) {
                return _.uniq(issues, function(issue) {
                    return issue.id;
                });
            });
    },

    persist: function(token, namespace, project, issue) {
        var url = this.url(namespace, project, issue.id),
            formatter = this.formatter,
            timerPut = this.container.get('statsd').createTimer('gitlab.put'),
            timerRoute = this.container.get('statsd').createTimer('gitlab.put.issue');

        return this.client.put(token, url, formatter.formatIssueToGitlab(issue))
            .then(function(response) {
                timerPut.stop();
                timerRoute.stop();

                if (response.statusCode !== 200) {
                    throw response;
                }

                return formatter.formatIssueFromGitlab(response.body);
            });
    },

    close: function(token, namespace, project, issue) {
        issue.state_event = 'close';

        return this.persist(token, namespace, project, issue);
    }
};
